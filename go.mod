module gitlab.com/gitlab-org/security-products/analyzers/eslint/v2

require (
	github.com/logrusorgru/aurora v0.0.0-20181002194514-a7b3b318ed4e
	github.com/urfave/cli v1.22.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.9.1
	gitlab.com/gitlab-org/security-products/analyzers/eslint v1.0.1
)

go 1.13
